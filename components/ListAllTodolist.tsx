import { Todolist } from '../interfaces';
import { Flex, Icon, Text, Box, Checkbox, Stack } from '@chakra-ui/react';
import { IoEllipse } from 'react-icons/io5';
import React, { useState } from 'react';
import ButtonDeleteTodolist from './ButtonDeleteTodolist';
import ButtonEditTodolist from './ButtonEditTodolist';

interface ListTodolistProps {
	dataListTodolist: Todolist[];
}

const ListAllTodolist: React.FC<ListTodolistProps> = ({ dataListTodolist }) => {
	const [checkedItems, setCheckedItems] = useState<number[]>([]);

	const handleCheckboxChange = (itemId: number) => {
		setCheckedItems((prevCheckedItems) => {
			if (prevCheckedItems.includes(itemId)) {
				return prevCheckedItems.filter((id) => id !== itemId);
			} else {
				return [...prevCheckedItems, itemId];
			}
		});
	};

	const listTagColor: Record<string, string> = {
		'very-high': '#ED4C5C',
		high: '#F8A541',
		normal: '#00A790',
		low: '#428BC1',
		'very-low': '#8942C1',
	};

	return (
		<Stack spacing={3} data-cy="activity-item">
			{dataListTodolist?.map((item) => (
				<Flex
					w="full"
					p={7}
					rounded="xl"
					alignItems="center"
					boxShadow="0px 6px 10px rgba(0, 0, 0, 0.1);"
					justifyContent="space-between"
					alignContent="center"
					key={item.id}
				>
					<Flex alignItems="center">
						<Checkbox
							size="lg"
							onChange={() => handleCheckboxChange(item.id)}
							isChecked={checkedItems.includes(item.id)}
						/>

						<Icon
							as={IoEllipse}
							color={listTagColor[item.priority] ?? 'red.600'}
							w="6"
							h="6"
							mx={4}
						/>
						<Text
							as="span"
							textDecoration={
								checkedItems.includes(item.id) ? 'line-through' : ''
							}
							mr="4"
						>
							{item.title} {item.id}
						</Text>
						<ButtonEditTodolist listData={item} id={item.id} />
					</Flex>
					<Box>
						<ButtonDeleteTodolist
							listData={item}
							title={item.title}
							id={item.id}
						/>
					</Box>
				</Flex>
			))}
		</Stack>
	);
};

export default ListAllTodolist;
