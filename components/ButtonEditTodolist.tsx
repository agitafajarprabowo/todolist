import { useForm } from 'react-hook-form';
import {
	Box,
	Text,
	Button,
	Flex,
	FormControl,
	Icon,
	Input,
	Modal,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	Select,
	useDisclosure,
} from '@chakra-ui/react';
import { BsPencil } from 'react-icons/bs';
import { Todolist } from '../interfaces';
import { usePatchTodolist } from '../http/Todolist/hooks/usePatchTodolist';
import { useRefetchTodolist } from '../http/Todolist/hooks/useRefetchTodolist';
import { zodResolver } from '@hookform/resolvers/zod';
import { z } from 'zod';

interface TodolistProps {
	listData: Todolist;
	onFormSubmitted?: () => void;
	id: number;
}

const todolistFormSchema = z.object({
	title: z.string().min(1, { message: 'Judul harus diisi' }),
	priority: z.string().min(1, { message: 'Prioritas harus diisi' }),
});

type TodolistFormData = z.infer<typeof todolistFormSchema>;

const ButtonEditTodolist: React.FC<TodolistProps> = ({
	listData,
	onFormSubmitted,
	id,
}) => {
	const { isOpen, onOpen, onClose } = useDisclosure();
	const { refetchTodolistView } = useRefetchTodolist();
	const { mutateAsync: mutatePatchTodolist } = usePatchTodolist();
	const defaultValues = listData;

	const {
		register,
		handleSubmit,
		reset,
		formState: { isSubmitting },
	} = useForm<TodolistFormData>({
		defaultValues,
		resolver: zodResolver(todolistFormSchema),
	});

	const formSubmit = handleSubmit(async (data) => {
		const updatedValues = {
			...defaultValues,
			...data,
		};

		const updatedData = await mutatePatchTodolist({
			id: id,
			data: updatedValues,
		});

		if (updatedData) {
			reset(updatedData);
			await refetchTodolistView();
			onFormSubmitted?.();
		}
	});

	const listTag: Record<string, string> = {
		'very-high': 'Very High',
		high: 'High',
		normal: 'Normal',
		low: 'Low',
		'very-low': 'Very Low',
	};

	return (
		<Box>
			<Icon
				cursor="pointer"
				as={BsPencil}
				color="#A4A4A4"
				w="4"
				h="4"
				onClick={onOpen}
			/>
			<Modal isOpen={isOpen} onClose={onClose} isCentered size="4xl">
				<ModalOverlay />
				<ModalContent py="4">
					<form onSubmit={formSubmit}>
						<ModalHeader
							borderBottomColor="#E5E5E5"
							borderBottomWidth="1px"
							px="8"
						>
							<Flex justifyContent="space-between">
								<Text>Tambah List Item</Text>
								<ModalCloseButton mt="4" mr="4" size="lg" color="#A4A4A4" />
							</Flex>
						</ModalHeader>
						<ModalBody px="8">
							<FormControl my="8">
								<Text fontSize="xs" fontWeight="bold" mb="2">
									NAMA LIST ITEM
								</Text>
								<Input
									size="lg"
									type="text"
									name="title"
									{...register('title')}
									placeholder="Tambahkan nama Activity"
								/>
							</FormControl>
							<FormControl mt={4}>
								<Text fontSize="xs" fontWeight="bold" mb="2">
									PRIORITY
								</Text>
								<Select
									size="lg"
									name="priority"
									{...register('priority')}
									w="205px"
								>
									{Object.entries(listTag).map(([priority, tag]) => (
										<option key={priority} value={priority}>
											{tag}
										</option>
									))}
								</Select>
							</FormControl>
						</ModalBody>
						<ModalFooter
							borderTopColor="#E5E5E5"
							borderTopWidth="1px"
							mt="4"
							pt="6"
						>
							<Button
								variant="ghost"
								rounded="full"
								bg="#16ABF8"
								w="140px"
								h="50px"
								fontSize="lg"
								color="white"
								_hover={{ bg: '#16ABF8' }}
								colorScheme="blue"
								type="submit"
								onClick={onClose}
								disabled={isSubmitting || !register}
							>
								Simpan
							</Button>
						</ModalFooter>
					</form>
				</ModalContent>
			</Modal>
		</Box>
	);
};

export default ButtonEditTodolist;
