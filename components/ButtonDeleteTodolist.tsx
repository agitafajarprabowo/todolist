import {
	Button,
	Icon,
	Modal,
	ModalBody,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	useDisclosure,
} from '@chakra-ui/react';
import * as React from 'react';
import { IoTrashOutline, IoWarningOutline } from 'react-icons/io5';
import { useRefetchTodolist } from '../http/Todolist/hooks/useRefetchTodolist';
import { Todolist } from '../interfaces';
import { useDeleteTodolist } from '../http/Todolist/hooks/useDeleteTodolist';

interface DeleteTodolistProps {
	listData: Todolist;
	title: string;
	id: number;
}

const ButtonDeleteTodolist: React.FC<DeleteTodolistProps> = ({
	listData,
	title,
	id,
}) => {
	const { refetchTodolistView } = useRefetchTodolist();
	const { isOpen, onOpen, onClose } = useDisclosure();
	const { mutateAsync: deleteTodolist } = useDeleteTodolist();

	const handleDelete = async () => {
		await deleteTodolist(
			{
				params: listData,
			},
			{
				onSuccess: async () => {
					await refetchTodolistView();
					onClose();
				},
			}
		);
	};

	return (
		<>
			<Icon
				color="#888888"
				onClick={onOpen}
				as={IoTrashOutline}
				w={6}
				h={6}
				cursor="pointer"
			/>
			<Modal isOpen={isOpen} onClose={onClose} size="lg" isCentered>
				<ModalOverlay />
				<ModalContent px="12" py="2" rounded="xl">
					<ModalHeader
						justifyContent="center"
						display="flex"
						color="#ED4C5C"
						mt="2"
					>
						<Icon as={IoWarningOutline} w="20" h="20" />
					</ModalHeader>
					<ModalBody fontSize="lg" mb="10" mt="6" textAlign="center">
						Apakah anda yakin menghapus activity <b>“{title}”</b> ? {id}
					</ModalBody>
					<ModalFooter display="flex" justifyContent="center">
						<Button
							variant="ghost"
							mr={6}
							onClick={onClose}
							rounded="full"
							bg="#F4F4F4"
							w="140px"
							h="50px"
							fontSize="lg"
							color="#4A4A4A"
						>
							Batal
						</Button>
						<Button
							variant="ghost"
							mr={3}
							rounded="full"
							bg="#ED4C5C"
							w="140px"
							h="50px"
							fontSize="lg"
							color="white"
							_hover={{ bg: '#ED4C5C' }}
							onClick={handleDelete}
						>
							Delete
						</Button>
					</ModalFooter>
				</ModalContent>
			</Modal>
		</>
	);
};

export default ButtonDeleteTodolist;
