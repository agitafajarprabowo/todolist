import { useForm } from 'react-hook-form';
import { Activity } from '../interfaces';
import { Button, Icon } from '@chakra-ui/react';
import { IoAddSharp } from 'react-icons/io5';
import React from 'react';

interface ActivityProps {
	mutateAsync: (activity: Activity) => void;
	refetchAsync: () => void;
}

const ButtonAddActivity: React.FC<ActivityProps> = ({
	mutateAsync,
	refetchAsync,
}) => {
	const { register, handleSubmit } = useForm<Activity>();
	const [isLoading, setIsLoading] = React.useState<boolean>(false);

	const defaultValues: Activity = {
		email: 'agitafajarprabowo@gmail.com',
		title: 'New Activity',
		data: undefined,
	};

	const onFormSubmit = async (data: Activity) => {
		try {
			setIsLoading(true);
			await mutateAsync(data);
			await refetchAsync();
		} catch (error) {
			// Handle error if needed
		}
		setIsLoading(false);
	};

	return (
		<div>
			<form onSubmit={handleSubmit(onFormSubmit)}>
				<input
					type="email"
					{...register('email')}
					defaultValue={defaultValues.email}
					hidden
				/>
				<input
					type="text"
					{...register('title')}
					defaultValue={defaultValues.title}
					hidden
				/>
				<Button
					type="submit"
					bg=" #16ABF8"
					rounded="full"
					p="7"
					color="white"
					fontSize="xl"
					leftIcon={<Icon as={IoAddSharp} w={8} h={8} />}
					_hover={{ bg: '#16ABF8' }}
					isLoading={isLoading}
					data-cy="activity-add-button"
				>
					Tambah
				</Button>
			</form>
		</div>
	);
};

export default ButtonAddActivity;
