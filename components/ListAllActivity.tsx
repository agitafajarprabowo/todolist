import { Card, Flex, Link, Wrap, WrapItem, Text } from '@chakra-ui/react';
import React from 'react';
import { format } from 'date-fns';
import { AllActivity } from '../http/Activity/fetch/getAllActivity';
import ButtonDeleteActivity from './ButtonDeleteActivity';

interface ListActivityProps {
	dataList: AllActivity[];
}

const ListAllActivity: React.FC<ListActivityProps> = ({ dataList }) => {
	return (
		<Wrap spacing={12} w="5xl">
			{dataList?.map((item) => (
				<WrapItem key={item.id}>
					<Card
						p={6}
						h="234px"
						justifyContent="space-between"
						w="235px"
						boxShadow="0 4px 8px rgba(0,0,0,0.15)"
						mb="2"
					>
						<Link href={`/todolist?id=${item.id}&title=${item.title}`}>
							<Text
								h="150px"
								fontSize="lg"
								fontWeight="bold"
								data-cy="activity-item-title"
							>
								{item.title}
							</Text>
						</Link>
						<Flex
							justifyContent="space-between"
							color="#888888"
							alignItems="center"
						>
							<Text data-cy="activity-item-date">
								{format(new Date(item.created_at), 'dd MMM yyyy')}
							</Text>
							<ButtonDeleteActivity title={item.title} />
						</Flex>
					</Card>
				</WrapItem>
			))}
		</Wrap>
	);
};

export default ListAllActivity;
