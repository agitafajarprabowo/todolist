import { Box, Container } from '@chakra-ui/react';
import Head from 'next/head';
import React, { ReactNode } from 'react';

export default function BaseLayout({
	children,
	pageTitle,
}: {
	children: ReactNode;
	pageTitle: string;
}) {
	return (
		<>
			<Head>
				<title>{pageTitle}</title>
				<meta charSet="utf-8" />
				<meta name="viewport" content="initial-scale=1.0, width=device-width" />
			</Head>
			<Box bg="#16ABF8" h="105px">
				<Container
					maxW="5xl"
					color="white"
					fontSize="2xl"
					fontWeight="bold"
					h="full"
					display="flex"
					alignItems="center"
					bg="#16ABF8"
					data-cy="header-title"
				>
					TO DO LIST APP
				</Container>
			</Box>
			<Container maxW="5xl">{children}</Container>
		</>
	);
}
