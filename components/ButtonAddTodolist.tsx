import { useForm } from 'react-hook-form';
import {
	Box,
	Text,
	Button,
	Flex,
	FormControl,
	Icon,
	Input,
	Modal,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	Select,
	useDisclosure,
} from '@chakra-ui/react';
import { IoAddSharp } from 'react-icons/io5';
import React from 'react';
import { Todolist } from '../interfaces';

interface TodolistProps {
	mutateAsync: (todolist: Todolist) => void;
	refetchAsync?: () => void;
	id: string | string[];
}

const ButtonAddTodolist: React.FC<TodolistProps> = ({
	mutateAsync,
	refetchAsync,
	id,
}) => {
	const { register, handleSubmit, reset } = useForm<Todolist>();
	const [isLoading, setIsLoading] = React.useState<boolean>(false);
	const { isOpen, onOpen, onClose } = useDisclosure();
	const [isTitleEmpty, setIsTitleEmpty] = React.useState<boolean>(true);

	const onFormSubmit = async (data: Todolist) => {
		try {
			setIsLoading(true);
			await mutateAsync(data);
			await refetchAsync();
		} catch (error) {
			console.log(error);
		}
		setIsLoading(false);
		reset();
	};

	const handleTitleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
		setIsTitleEmpty(!event.target.value);
	};

	const listTag: Record<string, string> = {
		'very-high': 'Very High',
		high: 'High',
		normal: 'Medium',
		low: 'Low',
		'very-low': 'Very Low',
	};

	return (
		<Box>
			<Button
				type="submit"
				bg=" #16ABF8"
				rounded="full"
				p="7"
				color="white"
				fontSize="xl"
				leftIcon={<Icon as={IoAddSharp} w={8} h={8} />}
				_hover={{ bg: '#16ABF8' }}
				isLoading={isLoading}
				onClick={onOpen}
				data-cy="todo-add-button"
			>
				Tambah
			</Button>

			<Modal isOpen={isOpen} onClose={onClose} isCentered size="4xl">
				<ModalOverlay />
				<ModalContent py="4">
					<form onSubmit={handleSubmit(onFormSubmit)}>
						<ModalHeader
							borderBottomColor="#E5E5E5"
							borderBottomWidth="1px"
							px="8"
						>
							<Flex justifyContent="space-between">
								<Text>Tambah List Item</Text>
								<ModalCloseButton mt="4" mr="4" size="lg" color="#A4A4A4" />
							</Flex>
						</ModalHeader>
						<ModalBody px="8">
							<FormControl my="8" data-cy="modal-add-name-input">
								<Text
									fontSize="xs"
									fontWeight="bold"
									mb="2"
									data-cy="modal-add-name-title"
								>
									NAMA LIST ITEM
								</Text>
								<Input
									size="lg"
									type="text"
									name="title"
									{...register('title')}
									placeholder="Tambahkan nama Activity"
									onChange={handleTitleChange}
								/>
							</FormControl>
							<FormControl my="8">
								<Input
									size="lg"
									type="text"
									name="title"
									{...register('activity_group_id')}
									defaultValue={id}
									placeholder="Tambahkan nama Activity"
									hidden
								/>
							</FormControl>
							<FormControl mt={4}>
								<Text
									fontSize="xs"
									fontWeight="bold"
									mb="2"
									data-cy="modal-add-priority-name"
								>
									PRIORITY
								</Text>
								<Select
									size="lg"
									name="priority"
									{...register('priority')}
									w="205px"
									data-cy="modal-add-priority-title"
								>
									{Object.entries(listTag).map(([priority, tag]) => (
										<option key={priority} value={priority}>
											{tag}
										</option>
									))}
								</Select>
							</FormControl>
						</ModalBody>
						<ModalFooter
							borderTopColor="#E5E5E5"
							borderTopWidth="1px"
							mt="4"
							pt="6"
						>
							<Button
								variant="ghost"
								rounded="full"
								bg="#16ABF8"
								w="140px"
								h="50px"
								fontSize="lg"
								color="white"
								_hover={{ bg: '#16ABF8' }}
								colorScheme="blue"
								type="submit"
								onClick={onClose}
								isDisabled={isTitleEmpty}
								data-cy="modal-add-save-button"
							>
								Simpan
							</Button>
						</ModalFooter>
					</form>
				</ModalContent>
			</Modal>
		</Box>
	);
};

export default ButtonAddTodolist;
