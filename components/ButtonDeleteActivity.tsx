import {
	Button,
	Icon,
	Modal,
	ModalBody,
	ModalContent,
	ModalFooter,
	ModalHeader,
	ModalOverlay,
	useDisclosure,
} from '@chakra-ui/react';
import * as React from 'react';
import { useDeleteActivity } from '../http/Activity/hooks/useDeleteActivity';
import { useGetAllActivity } from '../http/Activity/hooks/useGetAllActivity';
import { IoTrashOutline, IoWarningOutline } from 'react-icons/io5';
import { useRefetchActivity } from '../http/Activity/hooks/useRefetchActivity';

interface DeleteActivityProps {
	title: string;
}

const ButtonDeleteActivity: React.FC<DeleteActivityProps> = ({ title }) => {
	const { isOpen, onOpen, onClose } = useDisclosure();

	const email = 'agitafajarprabowo@gmail.com';

	const { mutateAsync: deleteActivity } = useDeleteActivity();
	const { data: listActivity } = useGetAllActivity({
		email,
	});
	const { refetchActivityView } = useRefetchActivity();

	const handleDelete = async () => {
		await deleteActivity(
			{ params: listActivity.find((dataList) => dataList.id) },
			{
				onSuccess: async () => {
					await refetchActivityView();
					onClose();
				},
			}
		);
	};

	return (
		<>
			<Icon
				color="#888888"
				onClick={onOpen}
				as={IoTrashOutline}
				w={6}
				h={6}
				cursor="pointer"
				data-cy="activity-item-delete-button"
			/>
			<Modal
				isOpen={isOpen}
				onClose={onClose}
				size="lg"
				isCentered
				data-cy="todo-modal-delete"
			>
				<ModalOverlay />
				<ModalContent px="12" py="2" rounded="xl">
					<ModalHeader
						justifyContent="center"
						display="flex"
						color="#ED4C5C"
						mt="2"
					>
						<Icon as={IoWarningOutline} w="20" h="20" />
					</ModalHeader>
					<ModalBody fontSize="lg" mb="10" mt="6" textAlign="center">
						Apakah anda yakin menghapus activity <b>“{title}”</b> ?
					</ModalBody>
					<ModalFooter display="flex" justifyContent="center">
						<Button
							variant="ghost"
							mr={6}
							onClick={onClose}
							rounded="full"
							bg="#F4F4F4"
							w="140px"
							h="50px"
							fontSize="lg"
							color="#4A4A4A"
							data-cy="modal-delete-cancel-button"
						>
							Batal
						</Button>
						<Button
							variant="ghost"
							mr={3}
							rounded="full"
							bg="#ED4C5C"
							w="140px"
							h="50px"
							fontSize="lg"
							color="white"
							_hover={{ bg: '#ED4C5C' }}
							onClick={handleDelete}
							data-cy="modal-delete-confirm-button"
						>
							Delete
						</Button>
					</ModalFooter>
				</ModalContent>
			</Modal>
		</>
	);
};

export default ButtonDeleteActivity;
