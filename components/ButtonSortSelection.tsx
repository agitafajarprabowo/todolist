import {
	Box,
	Button,
	Circle,
	Icon,
	Menu,
	MenuButton,
	MenuItem,
	MenuList,
} from '@chakra-ui/react';
import {
	BsSortAlphaDown,
	BsSortAlphaDownAlt,
	BsSortDownAlt,
	BsSortUpAlt,
} from 'react-icons/bs';
import { BiSortAlt2 } from 'react-icons/bi';

interface ButtonSortSelectionProps {
	handleSortChange: (option: string) => void;
}

const ButtonSortSelection: React.FC<ButtonSortSelectionProps> = ({
	handleSortChange,
}) => {
	return (
		<Box mr="4">
			<Menu>
				<MenuButton
					as={Button}
					variant="outline"
					colorScheme="#888888"
					rounded="full"
					px="-6"
				>
					<Icon as={BiSortAlt2} color="#888888" w="6" h="6" />
				</MenuButton>
				<MenuList>
					<MenuItem onClick={() => handleSortChange('latest')}>
						<Icon
							as={BsSortDownAlt}
							mr="2"
							color=" #16ABF8;
"
						/>
						Terbaru
					</MenuItem>
					<MenuItem onClick={() => handleSortChange('oldest')}>
						<Icon
							as={BsSortUpAlt}
							mr="2"
							color=" #16ABF8;
"
						/>
						Terlama
					</MenuItem>
					<MenuItem onClick={() => handleSortChange('az')}>
						<Icon
							as={BsSortAlphaDown}
							mr="2"
							color=" #16ABF8;
"
						/>
						A-Z
					</MenuItem>
					<MenuItem onClick={() => handleSortChange('za')}>
						<Icon
							as={BsSortAlphaDownAlt}
							mr="2"
							color=" #16ABF8;
"
						/>
						Z-A
					</MenuItem>
					<MenuItem onClick={() => handleSortChange('activeOnly')}>
						<Icon
							as={BiSortAlt2}
							mr="2"
							color=" #16ABF8;
"
						/>
						Belum Selesai
					</MenuItem>
				</MenuList>
			</Menu>
		</Box>
	);
};

export default ButtonSortSelection;
