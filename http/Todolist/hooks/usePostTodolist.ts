import { useToast } from '@chakra-ui/react';
import { useMutation } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import {
	getSuccessToastConfigs,
	getFailToastConfigs,
} from '../../../lib/toastConfigs';
import { PostTodolistArgs, postTodolist } from '../fetch/postTodolist';

export const usePostTodolist = () => {
	const toast = useToast();

	return useMutation<string, AxiosError, PostTodolistArgs['data']>(
		(data) => {
			return postTodolist({ data });
		},
		{
			onSuccess: () => {
				toast(
					getSuccessToastConfigs({
						title: 'Sukses',
						description: 'Komentar berhasil dikirim',
					})
				);
			},
			onError: () => {
				toast(
					getFailToastConfigs({
						title: 'gagal',
						description: 'Komentar gagal dikirim',
					})
				);
			},
		}
	);
};
