import { useQuery } from '@tanstack/react-query';
import { GetAllTodolistParams, getAllTodolist } from '../fetch/getAllTodolist';

export const useGetAllTodolist = (params?: GetAllTodolistParams) => {
	return useQuery(['list-todolist', params], () => getAllTodolist(params), {
		keepPreviousData: true,
		enabled: !!params?.activity_group_id,
	});
};
