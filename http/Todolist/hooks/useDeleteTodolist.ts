import { useToast } from '@chakra-ui/react';
import { useMutation } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import {
	getSuccessToastConfigs,
	getFailToastConfigs,
} from '../../../lib/toastConfigs';
import { DeleteTodolistArgs, deleteTodolist } from '../fetch/deleteTodolist';

export const useDeleteTodolist = () => {
	const toast = useToast();

	const mutation = useMutation<unknown, AxiosError, DeleteTodolistArgs>(
		(args) => {
			return deleteTodolist({
				...args,
			});
		},
		{
			onSuccess: () => {
				toast(
					getSuccessToastConfigs({
						title: 'Anggota TPM Terhapus',
						description: 'Anggota TPM berhasil dihapus',
					})
				);
			},
			onError: () => {
				toast(
					getFailToastConfigs({
						title: 'Anggota Tidak Terhapus',
						description: 'Anggota gagal untuk dihapus',
					})
				);
			},
		}
	);

	return { ...mutation };
};
