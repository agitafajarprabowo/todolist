import { useRouter } from 'next/router';
import { useGetAllTodolist } from './useGetAllTodolist';

export const useRefetchTodolist = () => {
	const router = useRouter();
	const { id } = router.query;
	const activity_group_id = Number(id);

	const { data: dataListAllTodolist, refetch: refetchAllTodolist } =
		useGetAllTodolist({
			activity_group_id,
		});

	const { data: listActivity, refetch: refetchTodolistView } =
		useGetAllTodolist({
			activity_group_id,
		});

	const todolistActGrId = listActivity?.find((item) => item.activity_group_id);
	const todolistTitle = listActivity?.find((item) => item.title);
	const todolistPriority = listActivity?.find((item) => item.priority);

	return {
		todolistPriority,
		todolistTitle,
		todolistActGrId,
		refetchTodolistView,
	};
};
