import { useToast } from '@chakra-ui/react';
import { useMutation } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import {
	getSuccessToastConfigs,
	getFailToastConfigs,
} from '../../../lib/toastConfigs';
import { PatchTodolistArgs, patchTodolist } from '../fetch/patchTodolist';
import { Todolist } from '../../../interfaces';

export const usePatchTodolist = () => {
	const toast = useToast();

	return useMutation<Todolist, AxiosError, PatchTodolistArgs['data']>(
		(args) => {
			if (args.data.title && args.data.priority) {
				return patchTodolist({
					...args,
				});
			}
		},
		{
			onSuccess: () => {
				toast(
					getSuccessToastConfigs({
						title: 'Sukses',
						description: 'Komentar berhasil dikirim',
					})
				);
			},
			onError: () => {
				toast(
					getFailToastConfigs({
						title: 'gagal',
						description: 'Komentar gagal dikirim',
					})
				);
			},
		}
	);
};
