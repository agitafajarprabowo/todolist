import axios from 'axios';
import { Activity } from '../../../interfaces';
import { API_URL } from '../../../pages/api/api';

export const updateActivity = async (
	id: number,
	data: Activity
): Promise<Activity> => {
	const response = await axios.patch(`${API_URL}/activity-groups/${id}`, data);
	console.log('dataedit', data);
	return response.data;
};
