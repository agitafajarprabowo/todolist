import axios from 'axios';
import { API_URL, API_URL_TODO } from '../../../pages/api/api';

export interface PostTodolistArgs {
	data: {
		activity_group_id?: number;
		title?: string;
		priority?: string;
	};
}

export const postTodolist = async (args: PostTodolistArgs) => {
	const data = {
		...args.data,
	};

	const response = await axios.post(`${API_URL_TODO}/todo-items`, data);
	return response.data;
};
