import axios from 'axios';
import { API_URL_TODO } from '../../../pages/api/api';

export interface AllTodolist {
	data: any;
	id?: number;
	activity_group_id: number;
	title: string;
	is_active?: number;
	priority?: string;
	created_at?: string;
	updated_at?: string;
	deleted_at?: string;
}

export interface GetAllTodolistParams {
	activity_group_id?: number;
}

export interface GetAllTodolistArgs {
	params?: GetAllTodolistParams;
}

export const getAllTodolist = async (
	params?: GetAllTodolistParams
): Promise<AllTodolist[]> => {
	const response = await axios.get(`${API_URL_TODO}/todo-items`, {
		params: {
			activity_group_id: params?.activity_group_id,
		},
	});
	return response.data.data || [];
};
