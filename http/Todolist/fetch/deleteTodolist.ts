import axios from 'axios';
import { API_URL, API_URL_TODO } from '../../../pages/api/api';
import { Todolist } from '../../../interfaces';

export interface DeleteTodolist {
	id?: number;
	activity_group_id: number;
	title: string;
	is_active?: string;
	priority?: string;
	created_at?: string;
	updated_at?: string;
	deleted_at?: string;
}

export interface DeleteTodolistArgs {
	params: Todolist;
}

export const deleteTodolist = async (
	params?: DeleteTodolistArgs
): Promise<DeleteTodolist[]> => {
	const response = await axios.delete(
		`${API_URL_TODO}/todo-items/${params?.params.id}`
	);
	return response.data;
};
