import axios from 'axios';
import { Todolist } from '../../../interfaces';
import { API_URL_TODO } from '../../../pages/api/api';

export interface PatchTodolistArgs {
	data: Todolist;
}

export const patchTodolist = async (args: PatchTodolistArgs) => {
	const data: Todolist = {
		...args.data,
	};

	const response = await axios.patch(
		`${API_URL_TODO}/todo-items/${data?.id}`,
		data
	);
	return response.data;
};
