import { useGetAllActivity } from './useGetAllActivity';

export const useRefetchActivity = () => {
	const email = 'agitafajarprabowo@gmail.com';
	const { data: listActivity, refetch: refetchActivityView } =
		useGetAllActivity({
			email,
		});

	const activityId = listActivity?.find((item) => item.id);

	const activityTitle = listActivity?.find((item) => item.title);

	return {
		activityTitle,
		activityId,
		refetchActivityView,
	};
};
