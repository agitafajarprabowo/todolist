import { useToast } from '@chakra-ui/react';
import { useMutation } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import {
	getSuccessToastConfigs,
	getFailToastConfigs,
} from '../../../lib/toastConfigs';
import { PatchActivityArgs, patchActivity } from '../fetch/patchActivity';
import { Activity } from '../../../interfaces';

export const usePatchActivity = () => {
	const toast = useToast();

	return useMutation<Activity, AxiosError, PatchActivityArgs['data']>(
		(args) => {
			if (args.id && args.title) {
				return patchActivity({
					...args,
				});
			}
		},
		{
			onSuccess: () => {
				toast(
					getSuccessToastConfigs({
						title: 'Sukses',
						description: 'Komentar berhasil dikirim',
					})
				);
			},
			onError: () => {
				toast(
					getFailToastConfigs({
						title: 'gagal',
						description: 'Komentar gagal dikirim',
					})
				);
			},
		}
	);
};
