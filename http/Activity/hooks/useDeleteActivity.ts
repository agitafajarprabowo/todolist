import { useToast } from '@chakra-ui/react';
import { useMutation } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { DeleteActivityArgs, deleteActivity } from '../fetch/deleteActivity';
import {
	getSuccessToastConfigs,
	getFailToastConfigs,
} from '../../../lib/toastConfigs';

export const useDeleteActivity = () => {
	const toast = useToast();

	const mutation = useMutation<unknown, AxiosError, DeleteActivityArgs>(
		(args) => {
			return deleteActivity({
				...args,
			});
		},
		{
			onSuccess: () => {
				toast(
					getSuccessToastConfigs({
						title: 'Anggota TPM Terhapus',
						description: 'Anggota TPM berhasil dihapus',
					})
				);
			},
			onError: () => {
				toast(
					getFailToastConfigs({
						title: 'Anggota Tidak Terhapus',
						description: 'Anggota gagal untuk dihapus',
					})
				);
			},
		}
	);

	return { ...mutation };
};
