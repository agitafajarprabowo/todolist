import { useQuery } from '@tanstack/react-query';
import { GetAllActivityParams, getAllActivity } from '../fetch/getAllActivity';

export const useGetAllActivity = (params?: GetAllActivityParams) => {
	return useQuery(['list-activity', params], () => getAllActivity(params), {
		keepPreviousData: true,
		enabled: !!params?.email,
	});
};
