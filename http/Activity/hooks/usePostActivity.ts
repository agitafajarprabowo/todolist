import { useToast } from '@chakra-ui/react';
import { useMutation } from '@tanstack/react-query';
import { AxiosError } from 'axios';
import { PostActivityArgs, postActivity } from '../fetch/postActivity';
import {
	getSuccessToastConfigs,
	getFailToastConfigs,
} from '../../../lib/toastConfigs';

export const usePostActivity = () => {
	const toast = useToast();

	return useMutation<string, AxiosError, PostActivityArgs['data']>(
		(data) => {
			return postActivity({ data });
		},
		{
			onSuccess: () => {
				toast(
					getSuccessToastConfigs({
						title: 'Sukses',
						description: 'Komentar berhasil dikirim',
					})
				);
			},
			onError: () => {
				toast(
					getFailToastConfigs({
						title: 'gagal',
						description: 'Komentar gagal dikirim',
					})
				);
			},
		}
	);
};
