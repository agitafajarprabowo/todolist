import axios from 'axios';
import { API_URL } from '../../../pages/api/api';

export interface AllActivity {
	id?: number;
	email: string;
	title: string;
	created_at?: string;
	updated_at?: string;
	deleted_at?: string;
}

export interface GetAllActivityParams {
	email?: string;
}

export interface GetAllActivityArgs {
	params?: GetAllActivityParams;
}

export const getAllActivity = async (
	params?: GetAllActivityParams
): Promise<AllActivity[]> => {
	const response = await axios.get(`${API_URL}/activity-groups`, {
		params: {
			email: params?.email,
		},
	});
	return response.data.data || [];
};
