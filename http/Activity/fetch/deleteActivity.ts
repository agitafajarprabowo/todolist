import axios from 'axios';
import { API_URL } from '../../../pages/api/api';

export interface DeleteActivity {
	id?: number;
	email?: string;
	title: string;
	created_at?: string;
	updated_at?: string;
	deleted_at?: string;
}

export interface DeleteActivityArgs {
	params: DeleteActivity;
}

export const deleteActivity = async (
	params?: DeleteActivityArgs
): Promise<DeleteActivity[]> => {
	const response = await axios.delete(
		`${API_URL}/activity-groups/${params?.params.id}`
	);
	return response.data;
};
