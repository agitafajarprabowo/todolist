import axios from 'axios';
import { API_URL } from '../../../pages/api/api';

export interface PostActivityArgs {
	data: {
		title?: string;
		email?: string;
	};
}

export const postActivity = async (args: PostActivityArgs) => {
	const data = {
		...args.data,
	};

	const response = await axios.post(`${API_URL}/activity-groups`, data);
	return response.data;
};
