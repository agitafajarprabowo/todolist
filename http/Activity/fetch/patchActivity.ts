import axios from 'axios';
import { API_URL } from '../../../pages/api/api';
import { Activity } from '../../../interfaces';

export interface PatchActivityArgs {
	data: Activity;
}

export const patchActivity = async (args: PatchActivityArgs) => {
	const data: Activity = {
		...args.data,
	};

	const response = await axios.patch(`${API_URL}/activity-groups/${data?.id}`);
	return response.data;
};
