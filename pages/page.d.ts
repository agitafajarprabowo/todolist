import { NextPage } from 'next';
import { ComponentType, ReactElement, ReactNode } from 'react';

import { UserRole } from '@/types/Role';

export type NextPageWithLayout<P = unknown> = NextPage<P> & {
	getLayout?: (_page: ReactElement) => ReactNode;
	layout?: ComponentType;
	auth?: {
		role: UserRole;
	};
};
