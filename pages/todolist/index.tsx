import { Box, Flex, Icon, Input, Link, Center, Image } from '@chakra-ui/react';
import BaseLayout from '../../components/layout';
import { NextPageWithLayout } from '../page';
import { IoChevronBackSharp } from 'react-icons/io5';
import { useRouter } from 'next/router';
import { usePostTodolist } from '../../http/Todolist/hooks/usePostTodolist';
import ButtonAddTodolist from '../../components/ButtonAddTodolist';
import ListAllTodolist from '../../components/ListAllTodolist';
import { useGetAllTodolist } from '../../http/Todolist/hooks/useGetAllTodolist';
import { BsPencil } from 'react-icons/bs';
import { Activity } from '../../interfaces';
import { zodResolver } from '@hookform/resolvers/zod';
import { useForm } from 'react-hook-form';
import { z } from 'zod';
import { useRefetchActivity } from '../../http/Activity/hooks/useRefetchActivity';
import { updateActivity } from '../../http/Todolist/fetch/updateActivity';
import { useGetAllActivity } from '../../http/Activity/hooks/useGetAllActivity';
import { useState } from 'react';
import React from 'react';
import ButtonSortSelection from '../../components/ButtonSortSelection';
import { AllTodolist } from '../../http/Todolist/fetch/getAllTodolist';

interface ActivityProps {
	listData: Activity;
	id: number;
}

const activityFormSchema = z.object({
	title: z.string().min(1, { message: 'Judul harus diisi' }),
});

type ActivityFormData = z.infer<typeof activityFormSchema>;

const IndexTodolistPage: NextPageWithLayout<ActivityProps> = ({ listData }) => {
	const router = useRouter();
	const { id } = router.query;
	const { title: previousTitle } = router.query;
	const activity_group_id = Number(id);
	const defaultValues = listData;
	const { refetchActivityView } = useRefetchActivity();
	const [_, setActivity] = useState<Activity | null>(null);
	const [sortedDataList, setSortedDataList] = useState<AllTodolist[]>([]);

	const { register, handleSubmit, setValue } = useForm<ActivityFormData>({
		defaultValues,
		resolver: zodResolver(activityFormSchema),
	});

	const email = 'agitafajarprabowo@gmail.com';
	const { data: listAllActivity } = useGetAllActivity({ email });

	const handleUpdateActivityTitle = async (formData) => {
		const { title } = formData;
		setValue('title', title);

		if (listAllActivity) {
			const updatedActivity = await updateActivity(activity_group_id, {
				...listAllActivity,
				title,
				data: undefined,
			});
			setActivity(updatedActivity);
			await refetchActivityView();
		}
	};

	const { mutateAsync: mutatePostTodolist } = usePostTodolist();

	const { data: dataListAllTodolist, refetch: refetchAllTodolist } =
		useGetAllTodolist({
			activity_group_id,
		});

	const handleSortChange = async (option) => {
		try {
			const sortedList = [...dataListAllTodolist];

			if (option === 'latest') {
				sortedList.sort((a, b) => b.id - a.id);
			} else if (option === 'oldest') {
				sortedList.sort((a, b) => a.id - b.id);
			} else if (option === 'az') {
				sortedList.sort((a, b) => a.title.localeCompare(b.title));
			} else if (option === 'za') {
				sortedList.sort((a, b) => b.title.localeCompare(a.title));
			}
			setSortedDataList(sortedList);
			await refetchAllTodolist();
		} catch (error) {
			console.error('Error:', error);
		}
	};

	return (
		<Box data-cy="dashboard">
			<Flex justifyContent="space-between" alignItems="center" py="12">
				<Flex alignItems="center">
					<Link href="/" mr="6" pt="3">
						<Icon as={IoChevronBackSharp} w="8" h="8" />
					</Link>
					<Box as="form" onChange={handleSubmit(handleUpdateActivityTitle)}>
						<Input
							id="title-input"
							fontSize="4xl"
							fontWeight="bold"
							type="text"
							defaultValue={previousTitle}
							variant="unstyled"
							maxW={`${previousTitle?.length}ch`}
							_focus={{
								borderColor: 'blue.400',
								borderBottom: '1px solid',
								w: 'full',
							}}
							onChange={handleSubmit(handleUpdateActivityTitle)}
							{...register('title')}
							data-cy="todo-title"
						/>
					</Box>
					<Icon
						cursor="pointer"
						color="#A4A4A4"
						w="5"
						h="5"
						as={BsPencil}
						onClick={() => {
							document.getElementById('title-input').focus();
						}}
						data-cy="todo-title-edit-button"
					/>
				</Flex>
				<Flex alignItems="center">
					<ButtonSortSelection handleSortChange={handleSortChange} />
					<ButtonAddTodolist
						id={id}
						mutateAsync={mutatePostTodolist}
						refetchAsync={refetchAllTodolist}
					/>
				</Flex>
			</Flex>
			<Box>
				{dataListAllTodolist?.length === 0 ? (
					<Center data-cy="todo-empty-state">
						<form>
							<Image
								display="flex"
								alignItems="center"
								src="/pic/todo-empty-state.png"
								alt="Empty state"
							/>
						</form>
					</Center>
				) : (
					<ListAllTodolist
						dataListTodolist={
							sortedDataList.length > 0 ? sortedDataList : dataListAllTodolist
						}
					/>
				)}
			</Box>
		</Box>
	);
};

IndexTodolistPage.getLayout = (page) => {
	return <BaseLayout pageTitle="Todolist Apps">{page}</BaseLayout>;
};

export default IndexTodolistPage;
