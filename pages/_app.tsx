import '../styles/global.css';
import { ChakraBaseProvider, extendTheme } from '@chakra-ui/react';
import {
	Hydrate,
	QueryClient,
	QueryClientProvider,
} from '@tanstack/react-query';
import React from 'react';

export const theme = extendTheme({
	config: {
		initialColorMode: 'light',
		useSystemColorMode: false,
	},
});

const queryClient = new QueryClient();

export default function App({ Component, pageProps }) {
	const getLayout = Component.getLayout || ((page) => page);

	return (
		<QueryClientProvider client={queryClient}>
			<Hydrate state={pageProps.dehydratedState}>
				<ChakraBaseProvider theme={theme}>
					{getLayout(<Component {...pageProps} />)}
				</ChakraBaseProvider>
			</Hydrate>
		</QueryClientProvider>
	);
}
