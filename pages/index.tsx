import BaseLayout from '../components/layout';
import { Box, Image, Flex, Text, Center } from '@chakra-ui/react';
import { NextPageWithLayout } from './page';
import { useGetAllActivity } from '../http/Activity/hooks/useGetAllActivity';
import ButtonAddActivity from '../components/ButtonAddActivity';
import { usePostActivity } from '../http/Activity/hooks/usePostActivity';
import ListAllActivity from '../components/ListAllActivity';

const IndexPage: NextPageWithLayout = () => {
	const email = 'agitafajarprabowo@gmail.com';
	const { data: listAllActivity, refetch: refetchAllActivity } =
		useGetAllActivity({ email });
	const { mutateAsync: mutateListActivity } = usePostActivity();

	return (
		<Box data-cy="dashboard">
			<Flex justifyContent="space-between" alignItems="center" py="12">
				<Text fontSize="4xl" fontWeight="bold" data-cy="activity-title">
					Activity
				</Text>
				<ButtonAddActivity
					refetchAsync={refetchAllActivity}
					mutateAsync={mutateListActivity}
				/>
			</Flex>
			<Box>
				{listAllActivity?.length === 0 ? (
					<Center data-cy="activity-empty-state">
						<form>
							<Image
								display="flex"
								alignItems="center"
								src="/pic/activity-empty-state.png"
								alt="Empty state"
							/>
						</form>
					</Center>
				) : (
					<ListAllActivity dataList={listAllActivity} />
				)}
			</Box>
		</Box>
	);
};

IndexPage.getLayout = (page) => {
	return <BaseLayout pageTitle="Todolist Apps">{page}</BaseLayout>;
};

export default IndexPage;
